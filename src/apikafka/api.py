from flask import Blueprint
from flask_restful import Api

from src.apikafka.resources.Tweets import Tweets
from src.apikafka.resources.Users import Users

blueprint_app = Blueprint('main_app', __name__)
api = Api(blueprint_app)

api.add_resource(Users, '/users', endpoint='users')
api.add_resource(Tweets, '/tweets', '/tweets/<string:fil>/latest', endpoint='tweets')
