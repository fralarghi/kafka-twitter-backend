import datetime
import json
import re
import uuid

from dateutil.parser import parse
from flask_restful import reqparse, Resource

from src.utils.KafkaC import KafkaC
from src.utils.KafkaP import KafkaP

# Input parse
parser = reqparse.RequestParser()
parser.add_argument('authors', location='json', required=True)
parser.add_argument('content', location='json', required=True)
parser.add_argument('location', location='json', required=True)


class Tweets(Resource):
    def get(self, fil):
        try:
            fil = json.loads(fil)
            consumer = KafkaC('tweets', 'earliest').get()
            polled = consumer.poll(timeout_ms=1000)
            tweets = list()
            for tweet in list(polled.values())[0]:
                if parse(tweet.value['timestamp']) > (datetime.datetime.now() - datetime.timedelta(minutes=5)):
                    if not fil['tags'] or (fil['tags'] and fil['tags'] in tweet.value['tags']):
                        if not fil['mentions'] or (fil['mentions'] and fil['mentions'] in tweet.value['mentions']):
                            if not fil['location'] or (fil['location'] and fil['location'] in tweet.value['location']):
                                tweets.append(tweet.value)
            return tweets, 200
        except Exception as e:
            return {'error': str(e)}, 500

    def post(self):
        try:
            args = dict(parser.parse_args())
            args['timestamp'] = str(datetime.datetime.now())
            args['id'] = str(uuid.uuid4())
            args['tags'] = re.findall(r"#(\w+)", str(args['content']))
            args['mentions'] = re.findall(r"@(\w+)", str(args['content']))
            result = KafkaP().send('tweets', args)
            return {"inserted": True, "topic": result.topic, "offset": result.offset,
                    "partition": result.partition}, 201
        except Exception as e:
            return {'error': str(e)}, 500
