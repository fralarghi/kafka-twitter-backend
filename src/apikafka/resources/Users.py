from flask_restful import reqparse, Resource

from src.utils.KafkaP import KafkaP

# Input parse
parser = reqparse.RequestParser()
parser.add_argument('nickname', location='json', required=True)
parser.add_argument('location', location='json', required=True)


class Users(Resource):
    def post(self):
        try:
            args = dict(parser.parse_args())
            result = KafkaP().send('users', args)
            return {"inserted": True, "topic": result.topic, "offset": result.offset,
                    "partition": result.partition}, 201
        except Exception as e:
            return {'error': str(e)}, 500
