import uuid

from flask import Flask
from flask_cors import CORS

from src.apikafka.api import blueprint_app

flask_app = Flask(__name__)
cors = CORS(flask_app, resources={r"/*": {"origins": "*"}})

flask_app.register_blueprint(blueprint_app)
flask_app.secret_key = uuid.uuid4().hex

if __name__ == "__main__":
    flask_app.run(debug=True, host='0.0.0.0', port=5000)
