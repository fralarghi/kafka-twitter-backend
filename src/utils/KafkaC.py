import json

from kafka import KafkaConsumer


class KafkaC:
    def __init__(self, topic, reset):
        self.consumer = KafkaConsumer(topic,
                                      bootstrap_servers=['localhost:9092'],
                                      value_deserializer=lambda v: json.loads(v.decode('utf-8')),
                                      auto_offset_reset=reset)

    def get(self):
        return self.consumer
