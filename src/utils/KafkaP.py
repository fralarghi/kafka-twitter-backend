import json

from kafka import KafkaProducer
from kafka.errors import KafkaError


class KafkaP:
    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers='localhost:9092',
                                      value_serializer=lambda v: json.dumps(v).encode('utf-8'))

    def send(self, topic, args):
        future = self.producer.send(topic=topic, value=args)
        try:
            result = future.get(timeout=10)
            return result
        except KafkaError as e:
            raise e
