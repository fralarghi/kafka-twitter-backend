import json

from flask import Flask, Response
from flask_cors import CORS

from src.utils.KafkaC import KafkaC

app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})


@app.route("/stream")
def stream():
    consumer = KafkaC('tweets', 'latest').get()

    def event_stream():
        while True:
            polled = consumer.poll(timeout_ms=1000)
            if polled:
                for tweet in list(polled.values())[0]:
                    print(tweet.value)
                    yield "data: {}\n\n".format(json.dumps(tweet.value))
    return Response(event_stream(), mimetype="text/event-stream")


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5500)
