# Kafka-twitter
This is a backend developed with flask-restful and kafka-python to simulate a twitter like application.\
HOST variable is to substitute with server assigned or local ip.

There are 2 different servers:  

- REST API server with port 5000
- SSE server with port 5500

If you want to run these servers, you have to previously set up and run Kafka with 'tweets' and 'users' topics.

## REST API description

#### POST user
**endpoint**:  
HOST:5000/users  
**body**:
```
{
    "nickname": "new_name",
    "location": "location_name"
}
```
**returns example**:  
```
{
    "inserted": true,
    "topic": "users",
    "offset": Number,
    "partition": 0
}
```

#### POST tweet
**endpoint**:  
HOST:5000/tweets  
**body**:
```
{
    "authors": "new_name",
    "content": "text of the tweet #hashtag @mention"
    "location": "location_name"
}
```
**returns example**:  
```
{
    "inserted": true,
    "topic": "tweets",
    "offset": Number,
    "partition": 0
}
```

#### GET tweets
###### (in the last 5 minutes, eventually filtered)
**endpoint**:  
HOST:5000/tweets/{"tags":"", "mentions":"","location":""}/latest  
**returns example**:  
```
[
    {
        "authors": "paolorossi",
        "content": "Fantastic tool! #wow #lovethis @lucabianchi @marioverdi",
        "location": "Milano",
        "timestamp": "2019-05-23 12:49:52.152036",
        "id": "aebe9bd6-b7e1-4058-a2a2-85edad88f144",
        "tags": [
            "wow",
            "lovethis"
        ],
        "mentions": [
            "lucabianchi",
            "marioverdi"
        ]
    },
    ...
]
```

## SSE description
#### Plain Javascript connection example

```
<!DOCTYPE html>
<html>
<body>

<h1>SSE connection</h1>
<div id="result"></div>

<script>
if(typeof(EventSource) !== "undefined") {
  var source = new EventSource("http://HOST:5500/stream");
  source.onmessage = function(event) {
    document.getElementById("result").innerHTML += event.data + "<br>";
  };
} else {
	document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
}
</script>

</body>
</html>
```